# Google Drive - Insight
Create a desktop Google Drive application.

This supports uploads and downloads of files and access to other Googles apps within the app such as Google Docs

This is for Windows only at the moment, I will be including Linux and MacOS soon. You can use this tutorial as a base line of method used.

For the purpose of this tutorial, I will entering commands and referring to item names in `this format`.
This symbol `⌘` Refers to the Windows key.


# Requirements
- [Node.js](https://nodejs.org)
- [Git for Windows](https://gitforwindows.org/)
- [Electron](https://electronjs.org/)
- [Nativefier](https://github.com/jiahaog/nativefier) 


# Creating the app
After installing all the requirement, deploying the task to execute the creation of the Google Drive application is simple.

Create a folder in your desired location in which you want to store the newly created application.
`Right Click` and Select `Git Bash Here`.

![Screen Shot 1](https://image.ibb.co/f98NFo/Screenshot_4.png "Screen Shot 1")

Once the Command Line Console has launched

Type in or paste by doing `Shift + Ins`.
```
nativefier "https://drive.google.com/"
```
Your console should look something like this

![Screen Shot 2](https://image.ibb.co/eP1bao/Screenshot_5.png "Screen Shot 2")

Now, navigate to the folder in which you opened Git Bash in.
You should now see a folder called something like `Meet Google Drive  One place for all your files-win32-x64`.

I recommend that you rename the folder to `Drive` and move it to `C:\Program Files (x86)\Google\`. 
The root directory where the application is should be `C:\Program Files (x86)\Google\Drive`.
In that folder, you should rename the executable file to `Google Drive`.

Now all you need to do is open the application and login with your Google account and you're all setup!


# Make the program feel more authentic on your computer
This step is optional.

For this part, we will make the application look more authentic on your computer.
This step is mainly for ease of use when using the application.

First of all, we will `Right Click` then hover over `Send to` then select `Desktop (create shortcut)`
This will create a short cut on your desktop. 

Now navigate you your desktop by doing `⌘ + D` and edit the name of the shortcut to remove ` - Shortcut`.

Open File Explorer and in the address bar enter `C:\ProgramData\Microsoft\Windows\Start Menu\Programs`.

![Screen Shot 3](https://image.ibb.co/k3vPT8/Screenshot_6.png "Screen Shot 3")

Copy the short cut from the desktop and paste it into this folder.

Now, whenever you go to the windows start menu, you can find in the list or search for the program.


That's it, you're finished. That's no joke. You can now enjoy the newly created Google Drive app.


# Useful information

You can now use the desktop app as if it was open in your web browser. This includes "Drag and Drop" of files.

When you download files, they will download to your default downloads folder.

If you have any problems or need any help or if you see anything wrong, please let me know, I'll be happy to help.

You can always contact me on discord by adding `Jacob.T#0001`


# Disclaimer

I do not own any of the software used in this tutorial.

Create and use the Google Drive app at your own risk, I will not be liable of any issues which may occur as a result of you not following this, or any other tutorial involving the applications used.

I am not affiliated directly or indirectly with Google, Inc.

For more information about this application, please look at the Terms and Conditions from the app vendors.
